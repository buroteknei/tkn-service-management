package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.BidAssignmentRequestDTO;
import com.teknei.bid.dto.BidDispDTO;
import com.teknei.bid.dto.BidEmprDTO;
import com.teknei.bid.dto.BidUsuaDTO;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/admin")
public class BidManagementController {

    @Autowired
    private BidUsuaRepository bidUsuaRepository;
    @Autowired
    private BidEmprRepository bidEmprRepository;
    @Autowired
    private BidDispRepository bidDispRepository;
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private BidEmprUsuaRepository bidEmprUsuaRepository;
    @Autowired
    private BidEmprDispRepository bidEmprDispRepository;
    private static final Logger log = LoggerFactory.getLogger(BidManagementController.class);

    @PostConstruct
    private void init() {
        passwordEncoder = new BCryptPasswordEncoder();
    }

    @ApiOperation(value = "Finds the information of the assignments related to the customer", response = List.class)
    @RequestMapping(value = "/assign/companyUser/{idUser}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprUsua>> findCompanyFromUser(@PathVariable Long idUser) {
        try {
            List<BidEmprUsua> list = bidEmprUsuaRepository.findAllByIdUsua(idUser);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding assignments in company for: {} with message: {}", idUser, e.getMessage());
            return new ResponseEntity<>((List<BidEmprUsua>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    @ApiOperation(value = "Assigns the relation between customer and company", notes = "Only idCompany and idCustomer values of the request are needed", response = Boolean.class)
    @RequestMapping(value = "/assign/companyUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyUser(@RequestBody BidAssignmentRequestDTO requestDTO) {
        BidEmprUsua bidEmprUsua = new BidEmprUsua();
        bidEmprUsua.setIdEmpr(requestDTO.getIdCompany());
        bidEmprUsua.setIdUsua(requestDTO.getIdCustomer());
        bidEmprUsua.setUsrCrea(requestDTO.getUsername());
        bidEmprUsua.setUsrOpeCrea(requestDTO.getUsername());
        bidEmprUsua.setIdTipo(3);
        bidEmprUsua.setIdEsta(1);
        bidEmprUsua.setFchCrea(new Timestamp(System.currentTimeMillis()));
        try {
            bidEmprUsuaRepository.save(bidEmprUsua);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error assigning company to user: {} with error: {}", requestDTO, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds the information on assignments related to the device", response = List.class)
    @RequestMapping(value = "/assign/companyDevice/{idDevice}", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDisp>> findCompanyFromDevice(@PathVariable Long idDevice) {
        try {
            List<BidEmprDisp> list = bidEmprDispRepository.findAllByIdDisp(idDevice);
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding assignments in company for: {} with message:{}", idDevice, e.getMessage());
            return new ResponseEntity<>((List<BidEmprDisp>) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Assigns the relation between customer and device", notes = "Only idCompany and idDisp values of the request are needed", response = Boolean.class)
    @RequestMapping(value = "/assign/companyDevice", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> assignCompanyDevice(@RequestBody BidAssignmentRequestDTO requestDTO) {
        BidEmprDisp bidEmprDisp = new BidEmprDisp();
        bidEmprDisp.setIdDisp(requestDTO.getIdDisp());
        bidEmprDisp.setIdEmpr(requestDTO.getIdCompany());
        bidEmprDisp.setUsrCrea(requestDTO.getUsername());
        bidEmprDisp.setUsrOpeCrea(requestDTO.getUsername());
        bidEmprDisp.setIdTipo(3);
        bidEmprDisp.setIdEsta(1);
        bidEmprDisp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        try {
            bidEmprDispRepository.save(bidEmprDisp);
            return new ResponseEntity<>(true, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error assigning company to device: {} with error: {}", requestDTO, e.getMessage());
            return new ResponseEntity<>(false, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds all the active records", notes = "The response is in form of a list of resources", response = BidUsuaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is sent successfully to the client"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/usua", method = RequestMethod.GET)
    public ResponseEntity<List<BidUsuaDTO>> findAllActive() {
        List<BidUsuaDTO> list = new ArrayList<>();
        try {
            List<BidUsua> listUsua = bidUsuaRepository.findByIdEsta(1);
            listUsua.forEach(e -> list.add(transform(e)));
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding usua: {}", e.getMessage());
            return new ResponseEntity<>(list, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Creates a new record", notes = "The response is the resource created", response = BidUsuaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be created")
    })
    @RequestMapping(value = "/usua", method = RequestMethod.POST)
    public ResponseEntity<BidUsuaDTO> insertUsua(@RequestBody BidUsuaDTO dto) {
        BidUsua usua = new BidUsua();
        usua.setFchCrea(new Timestamp(System.currentTimeMillis()));
        usua.setIdEsta(1);
        usua.setIdTipo(3);
        usua.setUsrOpeCrea(dto.getUsername());
        String encoded = passwordEncoder.encode(dto.getPassword());
        usua.setPass(encoded);
        usua.setUsrCrea(dto.getUsername());
        usua.setUsua(dto.getUsua());
        if (dto.getIdUsua() != null) {
            usua.setIdClie(dto.getIdUsua());
        }
        try {
            BidUsua created = bidUsuaRepository.save(usua);
            BidUsuaDTO transformed = transform(created);
            return new ResponseEntity<>(transformed, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error creating usua: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Updates a record", notes = "The response is the resource updated", response = BidUsuaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be saved")
    })
    @RequestMapping(value = "/usua", method = RequestMethod.PUT)

    public ResponseEntity<BidUsuaDTO> modifyUsua(@RequestBody BidUsuaDTO dto) {
        try {
            BidUsua usua = bidUsuaRepository.findOne(dto.getIdUsua());
            usua.setFchModi(new Timestamp(System.currentTimeMillis()));
            usua.setUsrModi(dto.getUsername());
            usua.setUsrOpeModi(dto.getUsername());
            String encoded = passwordEncoder.encode(dto.getPassword());
            usua.setPass(encoded);
            bidUsuaRepository.save(usua);
            BidUsua created = bidUsuaRepository.findOne(dto.getIdUsua());
            BidUsuaDTO transformed = transform(created);
            return new ResponseEntity<>(transformed, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error replacing usua: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Deleted a record", notes = "The response is the resource deleted", response = BidUsuaDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is deleted successfully"),
            @ApiResponse(code = 422, message = "The resource could not be deleted")
    })
    @RequestMapping(value = "/usua", method = RequestMethod.DELETE)
    public ResponseEntity<BidUsuaDTO> deleteUsua(@RequestBody BidUsuaDTO dto) {
        try {
            BidUsua usua = bidUsuaRepository.findOne(dto.getIdUsua());
            usua.setFchModi(new Timestamp(System.currentTimeMillis()));
            usua.setUsrModi(dto.getUsername());
            usua.setUsrOpeModi(dto.getUsername());
            usua.setIdEsta(2);
            bidUsuaRepository.save(usua);
            BidUsua created = bidUsuaRepository.findOne(dto.getIdUsua());
            BidUsuaDTO transformed = transform(created);
            return new ResponseEntity<>(transformed, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error deleting usua: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }


    @ApiOperation(value = "Finds all the active records", notes = "The response is in form of a list of resources", response = BidEmprDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is sent successfully to the client"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/empr", method = RequestMethod.GET)
    public ResponseEntity<List<BidEmprDTO>> findAllEmprActive() {
        List<BidEmprDTO> list = new ArrayList<>();
        try {
            List<BidEmpr> listEmpr = bidEmprRepository.findByIdEsta(1);
            listEmpr.forEach(e -> list.add(transform(e)));
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding empr: {}", e.getMessage());
            return new ResponseEntity<>(list, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Creates a new record", notes = "The response is the resource created", response = BidEmprDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be created")
    })
    @RequestMapping(value = "/empr", method = RequestMethod.POST)
    public ResponseEntity<BidEmprDTO> saveEmpr(@RequestBody BidEmprDTO dto) {
        BidEmpr bidEmpr = new BidEmpr();
        bidEmpr.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidEmpr.setIdEsta(1);
        bidEmpr.setIdTipo(3);
        bidEmpr.setNomEmpr(dto.getEmpr());
        bidEmpr.setUsrCrea(dto.getUsername());
        bidEmpr.setUsrOpeCrea(dto.getUsername());
        try {
            BidEmpr created = bidEmprRepository.save(bidEmpr);
            BidEmprDTO createdDTO = transform(created);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error creating empr: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Updates a record", notes = "The response is the resource updated", response = BidEmprDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be saved")
    })
    @RequestMapping(value = "/empr", method = RequestMethod.PUT)
    public ResponseEntity<BidEmprDTO> updateEmpr(@RequestBody BidEmprDTO dto) {
        try {
            BidEmpr created = bidEmprRepository.findOne(dto.getIdEmpr());
            created.setFchModi(new Timestamp(System.currentTimeMillis()));
            created.setUsrModi(dto.getUsername());
            created.setUsrOpeModi(dto.getUsername());
            created.setNomEmpr(dto.getEmpr());
            BidEmpr updated = bidEmprRepository.save(created);
            BidEmprDTO createdDTO = transform(updated);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error updating empr: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Deleted a record", notes = "The response is the resource deleted", response = BidEmprDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is deleted successfully"),
            @ApiResponse(code = 422, message = "The resource could not be deleted")
    })
    @RequestMapping(value = "/empr", method = RequestMethod.DELETE)
    public ResponseEntity<BidEmprDTO> deleteEmpr(@RequestBody BidEmprDTO dto) {
        try {
            BidEmpr created = bidEmprRepository.findOne(dto.getIdEmpr());
            created.setFchModi(new Timestamp(System.currentTimeMillis()));
            created.setUsrModi(dto.getUsername());
            created.setUsrOpeModi(dto.getUsername());
            created.setIdEsta(2);
            BidEmpr updated = bidEmprRepository.save(created);
            BidEmprDTO createdDTO = transform(updated);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error deleting empr: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Finds all the active records", notes = "The response is in form of a list of resources", response = BidDispDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is sent successfully to the client"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/disp", method = RequestMethod.GET)
    public ResponseEntity<List<BidDispDTO>> findAllDispActive() {
        List<BidDispDTO> list = new ArrayList<>();
        try {
            List<BidDisp> listDisp = bidDispRepository.findByIdEsta(1);
            listDisp.forEach(e -> list.add(transform(e)));
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding Disp: {}", e.getMessage());
            return new ResponseEntity<>(list, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Creates a new record", notes = "The response is the resource created", response = BidDispDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be created")
    })
    @RequestMapping(value = "disp", method = RequestMethod.POST)
    public ResponseEntity<BidDispDTO> createDisp(@RequestBody BidDispDTO dto) {
        BidDisp disp = new BidDisp();
        disp.setDescDisp(dto.getDescDisp());
        disp.setFchCrea(new Timestamp(System.currentTimeMillis()));
        disp.setIdEsta(1);
        disp.setIdTipo(3);
        disp.setNumSeri(dto.getNumSeri());
        disp.setUsrCrea(dto.getUsername());
        try {
            BidDisp created = bidDispRepository.save(disp);
            BidDispDTO createdDTO = transform(created);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error creating disp: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Updates a record", notes = "The response is the resource updated", response = BidDispDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is saved successfully"),
            @ApiResponse(code = 422, message = "The resource could not be saved")
    })
    @RequestMapping(value = "disp", method = RequestMethod.PUT)
    public ResponseEntity<BidDispDTO> updateDisp(@RequestBody BidDispDTO dto) {
        try {
            BidDisp created = bidDispRepository.findOne(dto.getIdDisp());
            created.setFchModi(new Timestamp(System.currentTimeMillis()));
            created.setUsrModi("tknpersapi");
            created.setNumSeri(dto.getNumSeri());
            created.setDescDisp(dto.getDescDisp());
            BidDisp updated = bidDispRepository.save(created);
            BidDispDTO createdDTO = transform(updated);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error updating disp: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiOperation(value = "Deleted a record", notes = "The response is the resource deleted", response = BidDispDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The response is deleted successfully"),
            @ApiResponse(code = 422, message = "The resource could not be deleted")
    })
    @RequestMapping(value = "disp", method = RequestMethod.DELETE)
    public ResponseEntity<BidDispDTO> deleteDisp(@RequestBody BidDispDTO dto) {
        try {
            BidDisp created = bidDispRepository.findOne(dto.getIdDisp());
            created.setFchModi(new Timestamp(System.currentTimeMillis()));
            created.setUsrModi("tknpersapi");
            created.setIdEsta(2);
            BidDisp updated = bidDispRepository.save(created);
            BidDispDTO createdDTO = transform(updated);
            return new ResponseEntity<>(createdDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error deleting disp: {}", e.getMessage());
            return new ResponseEntity<>(dto, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private BidDispDTO transform(BidDisp disp) {
        BidDispDTO dto = new BidDispDTO();
        dto.setDescDisp(disp.getDescDisp());
        dto.setIdDisp(disp.getIdDisp());
        dto.setNumSeri(disp.getNumSeri());
        return dto;
    }

    private BidUsuaDTO transform(BidUsua usua) {
        BidUsuaDTO dto = new BidUsuaDTO();
        dto.setIdUsua(usua.getIdUsua());
        dto.setUsua(usua.getUsua());
        return dto;
    }

    private BidEmprDTO transform(BidEmpr empr) {
        BidEmprDTO dto = new BidEmprDTO();
        dto.setEmpr(empr.getNomEmpr());
        dto.setIdEmpr(empr.getIdEmpr());
        return dto;
    }

}