package com.teknei.bid;

import com.teknei.bid.controller.rest.BidManagementController;
import com.teknei.bid.dto.BidAssignmentRequestDTO;
import com.teknei.bid.persistence.entities.BidEmprDisp;
import com.teknei.bid.persistence.entities.BidEmprDispPK;
import com.teknei.bid.persistence.entities.BidEmprUsua;
import com.teknei.bid.persistence.entities.BidEmprUsuaPK;
import com.teknei.bid.persistence.repository.BidEmprDispRepository;
import com.teknei.bid.persistence.repository.BidEmprUsuaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TknServiceManagementApplicationTests {

    private static final Logger log = LoggerFactory.getLogger(TknServiceManagementApplicationTests.class);

    @Autowired
    private BidManagementController bidManagementController;
    @Autowired
    private BidEmprDispRepository bidEmprDispRepository;
    @Autowired
    private BidEmprUsuaRepository bidEmprUsuaRepository;

    @Test
    public void contextLoad(){

    }

    //@Test
    public void testCipher() {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String password = "pass";
        String encoded = passwordEncoder.encode(password);
        log.info("Encoded: {}", encoded);
        org.junit.Assert.assertNotEquals(password, encoded);
    }

    //@Test
    //@Transactional
    public void testAssignments(){
        BidAssignmentRequestDTO assignmentRequestDTO = new BidAssignmentRequestDTO();
        final Long idCompanyInDB = 2l;
        final Long idCustomerInDB = 1l;
        final Long idDeviceOnDB = 1l;
        assignmentRequestDTO.setIdCompany(idCompanyInDB);
        assignmentRequestDTO.setIdCustomer(idCustomerInDB);
        assignmentRequestDTO.setIdDisp(idDeviceOnDB);
        ResponseEntity<Boolean> responseDevice = bidManagementController.assignCompanyDevice(assignmentRequestDTO);
        assertEquals(200, responseDevice.getStatusCodeValue());
        assertEquals(true, responseDevice.getBody());
        ResponseEntity<Boolean> responseUser = bidManagementController.assignCompanyUser(assignmentRequestDTO);
        assertEquals(200, responseUser.getStatusCodeValue());
        assertEquals(true, responseUser.getBody());
        BidEmprUsuaPK bidEmprUsuaPK = new BidEmprUsuaPK();
        bidEmprUsuaPK.setIdEmpr(idCompanyInDB);
        bidEmprUsuaPK.setIdUsua(idCustomerInDB);
        BidEmprUsua expected1 = bidEmprUsuaRepository.findOne(bidEmprUsuaPK);
        assertNotNull(expected1);
        BidEmprDispPK bidEmprDispPK = new BidEmprDispPK();
        bidEmprDispPK.setIdDisp(idDeviceOnDB);
        bidEmprDispPK.setIdEmpr(idCompanyInDB);
        BidEmprDisp expected2 = bidEmprDispRepository.findOne(bidEmprDispPK);
        assertNotNull(expected2);
        ResponseEntity<List<BidEmprDisp>> responseFindDevice = bidManagementController.findCompanyFromDevice(idDeviceOnDB);
        assertEquals(200, responseFindDevice.getStatusCodeValue());
        List<BidEmprDisp> listDevices = responseFindDevice.getBody();
        assertNotNull(listDevices);
        BidEmprDisp toFind = new BidEmprDisp();
        toFind.setIdEmpr(idCompanyInDB);
        toFind.setIdDisp(idDeviceOnDB);
        assertThat(listDevices, containsInAnyOrder(toFind));
        ResponseEntity<List<BidEmprUsua>> responseFindUsua = bidManagementController.findCompanyFromUser(idCustomerInDB);
        assertEquals(200, responseFindUsua.getStatusCodeValue());
        List<BidEmprUsua> listUsers = responseFindUsua.getBody();
        assertNotNull(listUsers);
        BidEmprUsua toFind2 = new BidEmprUsua();
        toFind2.setIdEmpr(idCompanyInDB);
        toFind2.setIdUsua(idCustomerInDB);
        assertThat(listUsers, containsInAnyOrder(toFind2));

    }

}
